jQuery ->
  $('#q_created_at_gt, #q_created_at_lt').datepicker
    format: "dd/mm/yyyy"
  $('#checkin_customer_name').autocomplete
  	source: "/checkins/customer_autocomplete"
  	select: (event,ui) -> $("#checkin_customer_id").val(ui.item.id)
  $("form#new_customer").on "ajax:success", (event, data, status, xhr) ->
    $("div#new_customer").modal "hide"
    $.ajax
      url: "/customers/customer_id_return/"
      data:
        customer_name: $("#customer_name").val()
      success: (data) ->
        $("#checkin_customer_id").val(data)
  $("form#new_customer").submit ->
   $("#checkin_customer_name").val $("#customer_name").val()
  $("#checkin_room_type").change ->
    $.ajax
      url: "/room_types/available_rooms/"
      data: 
        room_type: $("#checkin_room_type").val()
    $("#checkin_room_id").val("")
  $("#checkin_room_id").change ->
    $.ajax
      url: "/rooms/rate"
      data:
        room_id: $("#checkin_room_id").val()
      success: (data) ->
        $("#checkin_rate").val(data)
        $("#checkin_deposit").val(0)
        $("#checkin_discount").val(0)
        $("#checkin_prepaid").val(0)
