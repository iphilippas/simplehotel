# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('#q_created_at_gteq, #q_created_at_lteq').datepicker
    format: "dd/mm/yyyy"
