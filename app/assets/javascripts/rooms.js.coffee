# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.new_line_item
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $("body").on "ajax:success", "#new_line_item",->
    $.ajax
      dataType: "json",
      url: "/checkins/totalcost"
      data:
        checkin_id: $("#CheckinId").text()
      success: (data) ->
        $("#TotalCost").text(data.total_cost)
        $("#RoomCost").text(data.room_costs)
  $("body").on "ajax:success", ".delete-cost",->
    $(this).closest("tr").fadeOut()
    $.ajax
      dataType: "json",
      url: "/checkins/totalcost"
      data:
        checkin_id: $("#CheckinId").text()
      success: (data) ->
        $("#TotalCost").text(data.total_cost)
        $("#RoomCost").text(data.room_costs)
       
