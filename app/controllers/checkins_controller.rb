class CheckinsController < ApplicationController
  # GET /rooms
  # GET /rooms.json
  def index
    @q = Checkin.search(params[:q])
    @checkins = @q.result.where(" checked_out = ? ", false).paginate(:page => params[:page], :per_page => 10)		
		
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @checkins }
    end
  end

  def new
    @checkin = Checkin.new
    @customer = Customer.new
		@room_types = RoomType.all
				
		if params[:room_id].present?
		  @checkin.room_id = params[:room_id]
		  @checkin.rate = params[:rate]
		  @rooms = RoomType.find(@checkin.room.room_type).rooms.where("status = ?", false)
		else
		  @rooms = Room.where("status = ?", false)
		end
		@checkin.prepaid = @checkin.deposit = @checkin.discount = 0
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @checkins }
    end
  end
  
  def show
    @checkin = Checkin.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @checkin }
    end
  end
  
  def create
    @checkin = Checkin.new(params[:checkin])
    @customer = Customer.new()
    @room_types = RoomType.all
		@rooms = Room.all
            
    respond_to do |format|
      if @checkin.save
        format.html { redirect_to listrooms_path, notice: 'checkin was successfully created.' }
        format.json { render json: @checkin, status: :created, location: @checkin }
      else
        format.html { render action: "new" }
        format.json { render json: @checkin.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @checkin = Checkin.find(params[:id])
     
    respond_to do |format|
      if (@checkin.room_id != params[:checkin][:room_id])
           @checkin.open_room
      end
      if @checkin.update_attributes(params[:checkin])
        format.html { redirect_to checkins_path, notice: 'Checkin was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @checkin.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # GET /checkins/1/edit
  def edit
    @checkin = Checkin.find(params[:id])
    @room_types = RoomType.all
    @rooms = Room.where("room_type_id = ?", @checkin.room_type)
    @customer = Customer.new
  end
  
  def destroy
    @checkin = Checkin.find(params[:id])
    @checkin.destroy

    respond_to do |format|
      format.html { redirect_to checkins_url }
      format.json { head :no_content }
    end
  end
  
  #Autocomplete
  def customer_autocomplete
    if params[:term]
      like= "%".concat(params[:term].concat("%"))
      customers = Customer.where("name like ?", like)
    else
      customers = Customer.all
    end
    list = customers.map {|u| Hash[ id: u.id, label: u.name, name: u.name]}
   
    render json: list
  end
  #Ajax update costs
  def totalcost
    if !params[:checkin_id].blank?
      checkin = Checkin.find(params[:checkin_id])
    end
    
    render :json => { total_cost: checkin.total_cost, room_costs: checkin.room_costs }
  end
  
  def customer_name=(name)
		customer= Customer.find_by_name(name)
		if customer
			self.customer_id = customer.id
		else
			errors[:customer_name] << "Invalid name entered"
		end
	end

	def customer_name
  	Customer.find(customer_id).name if customer_id
	end
	
	# Checking out
	def checkout
	  @checkin = Checkin.find(params[:id])
		@invoice = Invoice.new({checkin_id:@checkin.id, rate:@checkin.total_cost})
		
		respond_to do |format|
      format.html 
      format.json 
    end
  end
  	
end
