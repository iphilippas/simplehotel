class InvoicesController < ApplicationController
  
  def index
    @q = Invoice.joins(:checkin).order("checkins.room_id asc").search(params[:q])
    @pdf_res = @q.result
    @invoices = @q.result.paginate(:page => params[:page], :per_page => 10)		
		@sum = Invoice.total_cost(@q.result)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invoices }
      format.pdf { render pdf: @pdf_res }
    end
  end
  
  def create
    @invoice = Invoice.new(params[:invoice])
    
    respond_to do |format|
      if @invoice.save
        @invoice.checkin.checkout
        format.html { redirect_to @invoice, notice: 'invoice was successfully created.' }
        format.json { render json: @invoice, status: :created, location: @invoice }
      else
        format.html { render action: "new" }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
    @invoice = Invoice.find(params[:id])
    
    respond_to do |format|
      format.html #html view
      format.pdf  { render :layout => true  }
		end
  end
end
