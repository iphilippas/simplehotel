class DashboardController < ApplicationController
  layout "dashboard"
  
  def index
    @rooms = Room.all
    @today = Date.today
    if params[:date]
      @date = Date.parse("1-#{params[:date]['month']}-#{params[:date]['year']}") 
    else
      @date = Date.today
    end
    
    @checkins = Checkin.joins(:room).where("fromdate <= ? AND todate >= ?", @date.end_of_month, @date.beginning_of_month).order("fromdate ASC").group_by{|t| t.room.name}
    
  end
end
