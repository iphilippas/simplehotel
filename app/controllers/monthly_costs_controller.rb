class MonthlyCostsController < ApplicationController
  # GET /monthly_costs
  # GET /monthly_costs.json
  def index
    @q = MonthlyCost.search(params[:q])
    @pdf_res=@q.result
    @monthly_costs = @q.result.paginate(:page => params[:page], :per_page => 20)
    @sum = MonthlyCost.total_cost(@pdf_res)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @monthly_costs }
      format.pdf { render :layout => true  }
    end
  end

  # GET /monthly_costs/1
  # GET /monthly_costs/1.json
  def show
    @monthly_cost = MonthlyCost.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @monthly_cost }
    end
  end

  # GET /monthly_costs/new
  # GET /monthly_costs/new.json
  def new
    @monthly_cost = MonthlyCost.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @monthly_cost }
    end
  end

  # GET /monthly_costs/1/edit
  def edit
    @monthly_cost = MonthlyCost.find(params[:id])
  end

  # POST /monthly_costs
  # POST /monthly_costs.json
  def create
    @monthly_cost = MonthlyCost.new(params[:monthly_cost])

    respond_to do |format|
      if @monthly_cost.save
        format.html { redirect_to @monthly_cost, notice: 'Monthly cost was successfully created.' }
        format.json { render json: @monthly_cost, status: :created, location: @monthly_cost }
      else
        format.html { render action: "new" }
        format.json { render json: @monthly_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /monthly_costs/1
  # PUT /monthly_costs/1.json
  def update
    @monthly_cost = MonthlyCost.find(params[:id])

    respond_to do |format|
      if @monthly_cost.update_attributes(params[:monthly_cost])
        format.html { redirect_to @monthly_cost, notice: 'Monthly cost was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @monthly_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /monthly_costs/1
  # DELETE /monthly_costs/1.json
  def destroy
    @monthly_cost = MonthlyCost.find(params[:id])
    @monthly_cost.destroy

    respond_to do |format|
      format.html { redirect_to monthly_costs_url }
      format.json { head :no_content }
    end
  end
end
