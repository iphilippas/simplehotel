class Room < ActiveRecord::Base
  belongs_to :room_type
  has_many :checkins
  has_many :invoices, :through => :checkins
  
  after_create :initial_status 
  
  attr_accessible :description, :name, :room_type_id, :status

  #Validations
  validates :name, presence:true
  #validates :rooms_type_id, presence:true
  
  def initial_status
    self.update_attribute(:status, false)
  end
  
 end
