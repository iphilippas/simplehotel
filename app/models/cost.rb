# encoding: UTF-8
class Cost < ActiveRecord::Base
  has_many :line_items
  has_many :checkins, :through => :line_items
  has_many :monthly_costs
  
  validates :name, :cost_type, presence: true 
  
  attr_accessible :description, :name, :cost_type, :cost_type_name
  attr_accessor :cost_type_name
  
  def cost_type_name
    if self.cost_type == 1
      return "Ειδικό"
    elsif self.cost_type == 2
      return "Γενικό"
    end
  end
end
