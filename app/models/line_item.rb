class LineItem < ActiveRecord::Base
  belongs_to :checkin
  belongs_to :cost
  has_one :invoice, :through => :checkin
  
  attr_accessible :rate, :checkin_id, :cost_id
  
  validates :rate, :cost_id, :checkin_id, presence: true
end
