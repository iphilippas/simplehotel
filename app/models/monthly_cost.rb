class MonthlyCost < ActiveRecord::Base
  belongs_to :cost
  attr_accessible :cost_id, :paid_at, :rate, :cost_name
  attr_accessor :cost_name
  
  default_scope order('paid_at DESC')
  
  def cost_name
    self.cost.name
  end
  
  def self.total_cost(costs)
        
    total = 0
    costs.each do |cost|
      total += cost.rate
    end
    total
  end
end
