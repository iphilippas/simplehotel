class Invoice < ActiveRecord::Base
	belongs_to :checkin
	has_one :customer, :through => :checkin
	has_one :room, :through => :checkin
	has_many :line_items, :through => :checkin
	
	after_create :set_checkin_true
	
  attr_accessible :checkin_id, :rate, :paid_method, :checkin_reference, :customer_name, :room_name
  attr_accessor :customer_name, :room_name
  
  def customer_name=(name)
		customer= Customer.find_by_name(name)
		if customer
			self.customer_id = customer.id
		else
			errors[:customer_name] << "Invalid name entered"
		end
	end

	def customer_name
  	#Customer.find(self.checkin.customer_id).name if self.checkin.customer_id
	  self.checkin.customer.name
	end
	
	def room_name
	  self.checkin.room.name
	end
	
	def self.total_cost(costs)
        
    total = 0
    costs.each do |cost|
      total += cost.checkin.clean_income
    end
    total
  end
  
  def set_checkin_true
    self.checkin.update_attribute(:paid, true)
  end
end
