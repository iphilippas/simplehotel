class RoomType < ActiveRecord::Base
  has_many :rooms
  attr_accessible :description, :name, :price

	#Validations
  validates :name, presence:true
  validates :price, presence:true
  validates :price, numericality:{greater_than_or_equal_to:0.01}
end
