class Statistic
  
  # Calculate all incomes per day
  def self.day_incomes(day)
    checkins = Checkin.joins(:invoice).where("fromdate <= ? AND todate >= ? AND paid = ?", day, day, true).group_by{|t| t.invoice.paid_method}
    income_cash = 0.0
    income_card = 0.0
    puts checkins
    if checkins[1]
      checkins[1].each do |checkin|
        income_cash += (checkin.rate - checkin.discount)
      end
    end
    if checkins[2]
      checkins[2].each do |checkin|
        income_card += (checkin.rate - checkin.discount)
      end
    end
    [income_cash, income_card, income_cash + income_card]
  end
  
  def self.day_extras(day)
    #day = day.to_date
    line_items = LineItem.joins(:checkin, :invoice).where("line_items.created_at >= ? and line_items.created_at <= ?  AND checkins.paid = ?", day, day.tomorrow, true).group_by{|t| t.invoice.paid_method}
    extras_cash  = 0.0
    extras_card  = 0.0
    
    if line_items[1]
      line_items[1].each do |line_item|
        extras_cash += line_item.rate
      end
    end
    
    if line_items[2]
      line_items[2].each do |line_item|
        extras_card += line_item.rate
      end
    end
    [extras_cash, extras_card, extras_cash + extras_card]
  end
  
  def self.monthly_costs(date)
    mcs = MonthlyCost.where("paid_at >= ? and paid_at <= ?", date.beginning_of_month, date.end_of_month)
    sum  = 0.0
    mcs.each do |mc|
      sum += mc.rate
    end
   sum
  end
end