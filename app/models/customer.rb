class Customer < ActiveRecord::Base
  has_many :checkins
  has_many :invoices, :through => :checkins
  
  attr_accessible :cellphone, :address, :email, :name, :telephone, :find_us, :vip

  validates :name, presence:true
  validates_format_of :email, :with => /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/, allow_blank: true
  validates :cellphone, :telephone, :numericality => {:only_integer => true}, allow_blank: true

 
end
