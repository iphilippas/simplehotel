# encoding: UTF-8
class Checkin < ActiveRecord::Base
  belongs_to :room
  belongs_to :customer
  
  has_many :line_items
  has_many :costs, :through => :line_items
  
  has_one :invoice
  
  after_create :save_reference
  after_update :set_room_inactive
  before_destroy :open_room
  
  
  attr_accessible :customer_id, :deposit, :description, :discount, :fromdate, :prepaid, :rate, :reference, :room_id, 
 	:todate, :customer_name, :room_type, :paid
	
	attr_accessor :customer_name, :room_type
	
	validates :customer_id, :rate, :room_id, :fromdate, :todate, presence: true

	validates_numericality_of :rate, :prepaid, :discount, :deposit
	validate :date_validation
	
	has_event_calendar :start_at_field  => 'fromdate', :end_at_field => 'todate'
	
	
	def date_validation
    if self.todate < self.fromdate
      errors.add(:todate, "Ημερομηνία")
      return false
    else
      return true
    end
  end
  
  def checkout
    self.update_attribute(:checked_out, true)
    self.room.update_attribute(:status, false)
  end
		
	def customer_name=(name)
		customer= Customer.find_by_name(name)
		if customer
			self.customer_id = customer.id
		else
			errors[:customer_name] << "Invalid name entered"
		end
	end

	def customer_name
  	Customer.find(customer_id).name if customer_id
	end
  
  def room_type
    room_type = Room.find(room_id).room_type_id if room_id
    RoomType.find(room_type).id if room_id 
  end
  
  
  def room_name
    Room.find(room_id).name if room_id
  end
  
  # Οικονομικά
  def room_rate
    days = (self.todate - self.fromdate).to_i + 1
    
    total = days * self.rate
  end
  
  def room_discount
    days = (self.todate - self.fromdate).to_i + 1
    
    total = days * self.discount
  end
  
  def room_costs
    costs = self.line_items
    final_cost = 0
    costs.each do |li| 
      final_cost += li.rate
    end
    return final_cost
  end
  
  def total_cost
    self.room_rate - (self.room_discount + self.prepaid + self.deposit) + self.room_costs
  end
  
  def clean_income
      self.room_rate - self.room_discount  + self.room_costs
  end
  
  def save_reference
    self.update_attribute(:reference, self.id.to_s + "-" + self.customer_name)
    self.update_attribute(:checked_out, false)
    self.room.update_attribute(:status, true)
  end
  
  def open_room
    self.room.update_attribute(:status, false)
  end
  
  def set_room_inactive
    if (self.room.status == false)
      self.room.update_attribute(:status, true)
    end
  end
  
  def cost_per_day
    
  end
    
end
