prawn_document() do |pdf|
   pdf.font("#{Prawn::BASEDIR}/data/fonts/DejaVuSans.ttf", :style => :normal)
   pdf.text "Απόδειξη #{@invoice.checkin.reference} - #{@invoice.checkin.customer.cellphone}"
   pdf.move_down(10)
	pdf.text "Από #{@invoice.checkin.fromdate} - Έως #{@invoice.checkin.todate}"
	pdf.move_down(10)
   invoicedata = [['Κόστος Δωματίου', "#{@invoice.checkin.room_rate}"],['Προπληρωμή',"#{@invoice.checkin.prepaid}"],['Εγγύηση', "#{@invoice.checkin.deposit}"],['Έκπτωση',"#{@invoice.checkin.discount}"]]
	pdf.table invoicedata,  :row_colors => ["d2e3ed", "FFFFFF"]
	pdf.move_down(10)
	
	pdf.text "Χρεώσεις δωματίου"
	pdf.move_down(10)
	
	@invoice.checkin.line_items.each do |item|
	  
	pdf.table [["#{item.created_at}", "#{item.rate}", "#{item.cost.name}"]],  :row_colors => ["d2e3ed", "FFFFFF"]
	end
	
	pdf.move_down(20)
	pdf.text "Σύνολο: #{@invoice.rate} - #{paid_method_string(@invoice)}"
end
