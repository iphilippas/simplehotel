prawn_document() do |pdf|
   pdf.font("#{Prawn::BASEDIR}/data/fonts/DejaVuSans.ttf", :style => :bold)
   pdf.text "Έσοδα"
   pdf.move_down(20)
	items =  [["Δωμάτιο", "Πελάτης", "Στις", "Πληρωτέο"]]
	
	items += @pdf_res.map do |item|
		item = ["#{item.checkin.room_name}", "#{item.checkin.customer_name}", "#{item.created_at}", "#{item.checkin.clean_income}"]
	end
	pdf.table items,  :row_colors => ["d2e3ed", "FFFFFF"], :header => true
	
	pdf.move_down(20)
	pdf.text "Σύνολο: #{@sum}"
	
end
