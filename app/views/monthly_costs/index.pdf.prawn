prawn_document() do |pdf|
   pdf.font("#{Prawn::BASEDIR}/data/fonts/DejaVuSans.ttf", :style => :normal)
   pdf.text "Έξοδα ξενοδοχείου"
   pdf.move_down(20)
	
	items = @pdf_res.map do |item|
		item = ["#{item.cost_name}", "#{item.rate}", "#{item.paid_at}"]
	end
	pdf.table items,  :row_colors => ["d2e3ed", "FFFFFF"]
	
	pdf.move_down(20)
	pdf.text "Σύνολο: #{@sum}"
	
end
