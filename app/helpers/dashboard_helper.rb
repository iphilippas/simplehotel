module DashboardHelper
  def room_column(rooms, checkins)
    html = ""
    
    rooms.each do |room|
      html += "<tr>"
      html += "<td class=\"hd\">#{room.name}</td>"
             
      
      for day in @date.beginning_of_month..@date.end_of_month
        if checkins["#{room.name}"]
          if checkin = checkins["#{room.name}"].detect { |checkin| (checkin.fromdate..checkin.todate).include?(day)}
            if checkin.invoice
              path = invoice_path(checkin.invoice)
              html_class = "paid"
            else
              path = manageroom_path(checkin.room)
              html_class = "pending"
            end
            html += "<td id=\"c_#{checkin.id}\" class=\"#{html_class} c\" data-content=\"#{checkin.description}\">
              <table>
                <tr><td>#{link_to checkin.customer.name, path}</td></tr>
                <tr><td>#{checkin.rate - checkin.discount }</td></tr>
               
              </table>
            </td>"
          else
            html += "<td></td>"
          end
        else
          html += "<td></td>"
        end
      end
      html += "</tr>"
    end
    
    html.html_safe
  end
end

