# encoding: UTF-8
module InvoicesHelper
  def paid_method_string(invoice)
    html = ''
    if invoice.paid_method == 1
      html = "Μετρητά"
    elsif invoice.paid_method == 2
      html = "Κάρτα"
    else
      html = ''
    end
  end
  
  def rate_info(invoice)
    html="<table>"
    html+="<tr><td class=\"imp\">Σύνολο</td><td class=\"imp\">#{invoice.checkin.clean_income}</td></tr>"
    html+="<tr><td class=\"imp\">Κόστος δωματίου</td><td>#{(invoice.checkin.todate - invoice.checkin.fromdate).to_i} x #{invoice.checkin.rate}</td></tr>"
    html+="<tr><td class=\"imp\">Έκπτωση</td><td>#{invoice.checkin.room_discount}</td></tr>"
    html+="<tr><td class=\"imp\">Extra</td><td>#{invoice.checkin.room_costs}</td></tr>"
    html+="</table>"
    html.html_safe
  end
end
