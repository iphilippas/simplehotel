require 'test_helper'

class MonthlyCostsControllerTest < ActionController::TestCase
  setup do
    @monthly_cost = monthly_costs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:monthly_costs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create monthly_cost" do
    assert_difference('MonthlyCost.count') do
      post :create, monthly_cost: { cost_id: @monthly_cost.cost_id, paid_at: @monthly_cost.paid_at, rate: @monthly_cost.rate }
    end

    assert_redirected_to monthly_cost_path(assigns(:monthly_cost))
  end

  test "should show monthly_cost" do
    get :show, id: @monthly_cost
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @monthly_cost
    assert_response :success
  end

  test "should update monthly_cost" do
    put :update, id: @monthly_cost, monthly_cost: { cost_id: @monthly_cost.cost_id, paid_at: @monthly_cost.paid_at, rate: @monthly_cost.rate }
    assert_redirected_to monthly_cost_path(assigns(:monthly_cost))
  end

  test "should destroy monthly_cost" do
    assert_difference('MonthlyCost.count', -1) do
      delete :destroy, id: @monthly_cost
    end

    assert_redirected_to monthly_costs_path
  end
end
