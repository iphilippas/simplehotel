class AddPaidToCheckins < ActiveRecord::Migration
  def change
    add_column :checkins, :paid, :boolean, default:false
  end
end
