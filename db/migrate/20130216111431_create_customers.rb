class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :address
      t.string :email
      t.string :telephone
      t.string :cellphone
      t.string :find_us
      t.boolean :vip

      t.timestamps
    end
  end
end
