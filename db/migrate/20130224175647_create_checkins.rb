class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
      t.integer :customer_id
      t.integer :room_id
      t.decimal :rate
      t.decimal :prepaid
      t.decimal :deposit
      t.decimal :discount
      t.date :fromdate
      t.date :todate
      t.text :description
      t.string :reference

      t.timestamps
    end
  end
end
