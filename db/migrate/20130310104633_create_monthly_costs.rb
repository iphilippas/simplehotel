class CreateMonthlyCosts < ActiveRecord::Migration
  def change
    create_table :monthly_costs do |t|
      t.integer :cost_id
      t.date :paid_at
      t.decimal :rate, :precision => 16, :scale => 2

      t.timestamps
    end
  end
end
