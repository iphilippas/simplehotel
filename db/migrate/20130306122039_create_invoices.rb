class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :checkin_id
      t.decimal :rate

      t.timestamps
    end
  end
end
