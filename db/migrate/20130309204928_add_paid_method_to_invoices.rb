class AddPaidMethodToInvoices < ActiveRecord::Migration
  def change
     add_column :invoices, :paid_method, :integer
  end
end
