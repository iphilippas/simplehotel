class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.string :name
      t.text :description
      t.integer :cost_type
      
      t.timestamps
    end
  end
end
