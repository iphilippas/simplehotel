SimpleHotel::Application.routes.draw do
  
  match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}

  get "statistics/all"

  resources :monthly_costs


  match "rooms/list_rooms" => 'rooms#list_rooms', as: :listrooms
  match "rooms/manage_room/:id" => "rooms#manage_room", as: :manageroom
  match "checkins/checkout/(:id)" => "checkins#checkout", as: :checkout
  match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  match "dashboard/index/(:date)" => "dashboard#index", as: :dashboard
  
  resources :customers do
    match :customer_id_return, :on => :collection
  end
  resources :costs
  resources :rooms do
  	match :rate, :on => :collection
  end
  resources :room_types do
    match :available_rooms, :on => :collection
  end
	

  resources :checkins do
    get :customer_autocomplete, :on => :collection
    get :totalcost, :on => :collection
   
  end
  
  resources :line_items
  resources :invoices
  resources :monthly_costs
  devise_for :users, :skip => :registrations

    
  root :to => 'dashboard#index'
end
